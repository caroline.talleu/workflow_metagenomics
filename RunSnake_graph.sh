#!/bin/bash
source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh

conda activate snakemake-6.9.1

mkdir -p report/

snakemake \
--snakefile $1 \
--dag \
| dot -Tpdf > ./report/`basename $1 .smk`_graph.pdf
