# Workflow Metagenomics

This pipeline is currently adapted for use on the [INRAE MIGALE](https://migale.inrae.fr/) cluster.

## Settings

In this folder, in a parent folder `../my_project` :

* Place the raw data in the folder `DATA/raw/` (or `../my_project/DATA/raw/`).
* Edit the file [`config.json`](config.json)
* Adapt the file [`cluster.json`](cluster.json)
* Select the desired outputs in the file [`global.smk`](global.smk)

## Running

For simple use :

```bash
./RunSnake.sh global.smk
```

For a dry-run :

```bash
./RunSnake_printshellcmds.sh global.smk
```

For a SGE submission :

```bash
./qsubSnake.sh global.smk
```

For a jobs visualisation :

```bash
./RunSnake_graph.sh global.smk
```

## Results

All results are available :

* In the folder `work/` for intermediate results,
* In the folder `report/` for reports.
